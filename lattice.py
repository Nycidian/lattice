__author__ = 'Nycidian'

import math
import inspect
import itertools


class Lattice(object):

    def __init__(self, steps=0, default_point=None,  point_functions=[],
                 distort=True):
        """
        Creates an Orthorhombic Primitive Lattice
        :param steps: a int value >= 0 which is equal to the number of rows, planes and columns
            including the zero of each and in the positive and negative directions.
        :param default_point: the value or object placed in each point when created, defaults to None
        :param point_functions: An optional function that can be called on each point
        """
        self.steps = steps

        self.default_point = default_point
        self.__point_functions__ = point_functions

        self.distort = distort

        self.__matrix__ = {}
        self.__create_matrix__()

    def __create_matrix__(self, exclude=None):
        for x, y, z in self.points(self.steps, exclude=exclude):
            self.__matrix__.setdefault(x, {}).setdefault(y, {})[z] = self.default_point

    def __return_arguments__(self, x, y, z, point, func):
        """
        Finds all the valid arguments in the passed method and returns a dictionary with mapped values
        """
        args = {}
        x_abs, y_abs, z_abs = self.absolute_index(x, y, z)
        step = max([x_abs, y_abs, z_abs])

        total_args = {
            'x': x,
            'y': y,
            'z': z,
            'x_abs': x_abs,
            'y_abs': y_abs,
            'z_abs': z_abs,
            'step': step,
            'point': point,
        }

        arg_names = inspect.getargspec(func)[0]

        for name in arg_names:
            args[name] = total_args[name]

        return args  # dict

    def __use_functions__(self, x, y, z, point):
        """
        For passed functions the correct argument names must be some combination of:
        x,
        y,
        z,
        x_abs,
        y_abs,
        z_abs,
        step,
        point,
        """

        for func in self.__point_functions__:

            kwargs = self.__return_arguments__(x, y, z, point, func)
            point = func(**kwargs)

        return point

    def __str__(self):

        temp_matrix = {}
        if self.distort:
            for x, y, z in self.points(self.steps):
                temp_matrix.setdefault(x, {}).setdefault(y, {})[z] = \
                    self.__use_functions__(x, y, z, self.__matrix__[x][y][z])
        else:
            for x, y, z in self.points(self.steps):
                temp_matrix.setdefault(x, {}).setdefault(y, {})[z] = self.__matrix__[x][y][z]
        return str(temp_matrix)

    def __bool__(self):
        """
        :return: Returns false if the only point present is 0,0,0
            and this point is mapped to a NoneTypeObject
        """
        if self.steps > 0:
            return True
        if self.__matrix__[0][0][0] is not None:
            return True
        return False

    def __nonzero__(self):      # Python 2 compatibility
        return type(self).__bool__(self)

    def __len__(self):
        return self.steps * 2 + 1

    def __handle_index__(self, key):
        if len(key) == 3:
            x, y, z = self.get_real_index(*key)
            return x, y, z
        else:
            raise IndexError

    def __getitem__(self, key):
        x, y, z = self.__handle_index__(key)

        if self.distort:
            return self.__use_functions__(x, y, z, self.__matrix__[x][y][z])
        else:
            return self.__matrix__[x][y][z]

    def __setitem__(self, key, point):
        x, y, z = self.__handle_index__(key)
        self.__matrix__[x][y][z] = point

    def __iter__(self):

        for x, y, z in self.points(self.steps):
            if self.distort:
                yield (x, y, z), self.__use_functions__(x, y, z, self.__matrix__[x][y][z])
            else:
                yield (x, y, z), self.__matrix__[x][y][z]

        raise StopIteration

    def __contains__(self, item):

        if math.fabs(item) <= self.steps:
            return True
        return False

    def __add__(self, other):
        """
        :param other: a Function or list of Functions to be added to point_methods
        """
        def add_func(f):
            if inspect.isfunction(f):
                self.__point_functions__.append(f)
            else:
                raise TypeError

        if isinstance(other, list):
            for func in other:
                add_func(func)
        else:
            add_func(other)

    def __sub__(self, other):
        """
        :param other: a Function or list of Functions to be removed from point_methods.
            This will remove the first instance of that function.
        """
        def sub_func(f):

            if inspect.isfunction(f):
                try:
                    self.__point_functions__.remove(f)
                except ValueError:
                    pass
            elif isinstance(f, str):
                remove = None
                for func in self.get_functions():
                    if func.func_name == f:
                        remove = func
                        break
                try:
                    self.__point_functions__.remove(remove)
                except ValueError:
                    pass
            else:
                raise TypeError

        if isinstance(other, list):
            for func in other:
                sub_func(func)
        else:
            sub_func(other)

    # As above
    def __radd__(self, other):
        self.__add__(other)

    def __rsub__(self, other):
        self.__sub__(other)

    def points(self, steps, x=None, y=None, z=None, exclude=None):
        """

        :param steps: how large teh lattice for these points
        :param x, y, z: if set to an int or list of ints this will be used instead of the total range
        :param exclude: int, all results up to this in in absolute terms will be excluded
        :yield: a tuple with x,y,z cords of a point
        """

        def normalize_axes(axi, point_step):

            if isinstance(axi, list):
                return axi
            if isinstance(axi, int):
                return [axi]
            elif axi is None:
                return range(-point_step, point_step + 1)
            else:
                raise TypeError

        x = normalize_axes(x, steps)
        y = normalize_axes(y, steps)
        z = normalize_axes(z, steps)

        for x_point in x:
            for y_point in y:
                for z_point in z:
                    if isinstance(exclude, int):
                        x_abs, y_abs, z_abs = self.absolute_index(x_point, y_point, z_point)
                        if x_abs <= exclude and y_abs <= exclude and z_abs <= exclude:
                            continue
                        else:
                            yield (x_point, y_point, z_point)
                    else:
                        yield (x_point, y_point, z_point)

        raise StopIteration

    @staticmethod
    def absolute_index(x, y, z):
        return int(math.fabs(x)), int(math.fabs(y)), int(math.fabs(z))

    def is_real(self, x, y, z):
        """
        Checks to see if an index is a real index.
        :param x, y, z: point index
        :return: bool
        """
        x_abs, y_abs, z_abs = self.absolute_index(x, y, z)
        step = max([x_abs, y_abs, z_abs])

        if step <= self.steps:
            return True
        return False

    def __modular_axes__(self, num):
        """
        Uses modular arithmetic to find real number of a mirror/real index
        """
        axi = num + self.steps
        axi %= (self.steps * 2) + 1
        num = axi - self.steps

        return num

    def get_mirror_points(self, x, y, z, mirror_depth=None):
        """
        Gives first order mirror indices for mirror points up to mirror_depth away
        """
        if mirror_depth is None:
            mirror_depth = self.steps*2 + 1
        elif mirror_depth > (self.steps*2 + 1):
            mirror_depth = self.steps*2 + 1

        indices = {}
        x, y, z = self.get_real_index(x, y, z)

        def mirror_axi(v):
            return [n for n in [v - mirror_depth, v, v + mirror_depth]
                    if -mirror_depth <= n <= mirror_depth]

        indices['x'] = mirror_axi(x)
        indices['y'] = mirror_axi(y)
        indices['z'] = mirror_axi(z)

        mirror_indices = list(itertools.product(indices['x'], indices['y'], indices['z']))
        mirror_indices.remove((x, y, z))

        if len(mirror_indices) == 0:
            raise StopIteration

        for x, y, z in mirror_indices:
            yield x, y, z

        raise StopIteration

    def get_real_index(self, x, y, z):
        if not isinstance(x, int) or not isinstance(y, int) or not isinstance(z, int):
                raise TypeError
        return self.__modular_axes__(x), self.__modular_axes__(y), self.__modular_axes__(z)

    def get_diagonals(self, x, y, z):
        """
        """
        x, y, z = self.get_real_index(x, y, z)
        diagonals = {}

        x_diag = range(x, x + len(self) + 1)
        x_diag_neg = range(x - (len(self)), x + 1)
        x_diag_neg.reverse()
        y_diag = range(y, y + len(self) + 1)
        y_diag_neg = range(y - (len(self)), y + 1)
        y_diag_neg.reverse()
        z_diag = range(z, z + len(self) + 1)

        x_right = []
        x_left = []
        y_right = []
        y_left = []
        z_right = []
        z_left = []

        for i in range(len(x_diag)):
            x_right.append(self.get_real_index(x, y_diag[i], z_diag[i]))
            x_left.append(self.get_real_index(x, y_diag_neg[i], z_diag[i]))
            y_right.append(self.get_real_index(x_diag[i], y, z_diag[i]))
            y_left.append(self.get_real_index(x_diag_neg[i], y, z_diag[i]))
            z_right.append(self.get_real_index(x_diag[i], y_diag[i], z))
            z_left.append(self.get_real_index(x_diag_neg[i], y_diag[i], z))

        diagonals['x_right'] = x_right
        diagonals['x_left'] = x_left
        diagonals['y_right'] = y_right
        diagonals['y_left'] = y_left
        diagonals['z_right'] = z_right
        diagonals['z_left'] = z_left

        for key, item in diagonals.items():
            yield key, item

        raise StopIteration

    def get_lines(self, x, y, z):
        """
        """
        x, y, z = self.get_real_index(x, y, z)
        lines = {}

        x_range = range(x, x + len(self) + 1)
        y_range = range(y, y + len(self) + 1)
        z_range = range(z, z + len(self) + 1)

        x_line = []
        y_line = []
        z_line = []

        for i in range(len(x_range)):
            x_line.append(self.get_real_index(x_range[i], y, z))
            y_line.append(self.get_real_index(x, y_range[i], z))
            z_line.append(self.get_real_index(x, y, z_range[i]))

        lines['x_line'] = x_line
        lines['y_line'] = y_line
        lines['z_line'] = z_line

        for key, item in lines.items():
            yield key, item

        raise StopIteration

    def apply_functions(self):

        for x, y, z in self.points(self.steps):
            if self.distort:
                self.__matrix__[x][y][z] = self.__use_functions__(x, y, z, self.__matrix__[x][y][z])
            else:
                pass

        self.__point_functions__ = []

    def get_functions(self):
        """
        :yield: function name, function, function args
        """

        for func in self.__point_functions__:
            yield func
        raise StopIteration

if __name__ == '__main__':
    pass