__author__ = 'Nycidian'

import unittest
import math

from lattice import Lattice


class TestMatrixZero(unittest.TestCase):

    def setUp(self):
        self.lattice_zero = Lattice(0, 'hello')

    def test_getitem(self):

        self.assertEqual(self.lattice_zero[0, 0, 0], 'hello')

    def test_set_distort(self):
        pass
        # TODO Add test

    def test_setitem(self):

        self.lattice_zero[0, 0, 0] = 'goodbye'
        self.assertEqual(self.lattice_zero[0, 0, 0], 'goodbye')

    def test_bool(self):

        self.assertTrue(bool(self.lattice_zero))
        self.lattice_zero[0, 0, 0] = None
        self.assertFalse(bool(self.lattice_zero))

    def test_len(self):

        self.assertEqual(len(self.lattice_zero), 1)

    def test_in(self):

        self.assertTrue(0 in self.lattice_zero)
        self.assertFalse(-1 in self.lattice_zero)

    def test_iter(self):

        self.lattice_zero[-1, 0, 0] = 'hello'

        for (x,y,z), point in self.lattice_zero:

            self.assertTrue(isinstance(x, int))
            self.assertTrue(isinstance(y, int))
            self.assertTrue(isinstance(z, int))
            self.assertTrue(isinstance(point, str))


def test(x, y, z, point):
        presence = point['presence']
        for n in reversed(range(1, 5)):
            end = 9-n
            if (x == -end or x == end) or (y == -end or y == end) or (z == -end or z == end):
                presence *= n/5.0
        return {'presence': presence, 'player': None}


def a(step):

    return {'step': step}


def b(step):

    return {'step': step}


def c(step):

    return {'step': step}


def d(step):

    return {'step': step}


class TestMatrixMore(unittest.TestCase):

    def setUp(self):
        self.lattice_zero = Lattice(8, {'presence': 1, 'player': None}, [test])

    def test_getitem(self):

        self.assertEqual(self.lattice_zero[0, 0, 0], {'presence': 1, 'player': None})

        with self.assertRaises(TypeError):
            self.lattice_zero['-11', 0, 0]

    def test_set_distort(self):
        pass
        # TODO Add test

    def test_setitem(self):

        self.lattice_zero[0, 0, 0] = {'presence': .5, 'player': None}
        self.assertEqual(self.lattice_zero[0, 0, 0], {'presence': .5, 'player': None})

        self.lattice_zero[8, 8, 8] = {'presence': .5, 'player': None}
        self.assertEqual(self.lattice_zero[8, 8, 8], {'presence': .1, 'player': None})

    def test_len(self):

        self.assertEqual(len(self.lattice_zero), 17)

class TestMatrixAlpha(unittest.TestCase):

    def setUp(self):
        self.lattice = Lattice(8, {}, [a, b, c, c, c])

    def test_get_functions(self):
        this = [func for func in self.lattice.get_functions()]
        self.assertEqual(this, [a, b, c, c, c])

    def test_add(self):

        self.lattice + a
        a + self.lattice

        this = [func for func in self.lattice.get_functions()]
        self.assertEqual(this, [a, b, c, c, c, a, a])

        self.lattice + [a, b, c]
        [a, b, c] + self.lattice

        this = [func for func in self.lattice.get_functions()]
        self.assertEqual(this, [a, b, c, c, c, a, a, a, b, c, a, b, c])

    def test_sub(self):

        self.lattice - c
        c - self.lattice

        this = [func for func in self.lattice.get_functions()]
        self.assertEqual(this, [a, b, c])

        self.lattice - [a, b]
        [c, c] - self.lattice

        this = [func for func in self.lattice.get_functions()]
        self.assertEqual(this, [])

if __name__ == '__main__':

    unittest.main()